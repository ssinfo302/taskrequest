import React, { Component } from 'react';
import {
    FlatList,
    StyleSheet,
    ScrollView,
    View,
    Text,
    Image,
    TouchableOpacity
} from 'react-native';
import { wp, hp } from '../helper/responsiveScreen';
import Constant from '../helper/themeHelper';
import topList from '../helper/topList.json';
import bottomList from '../helper/bottomList.json';
import Swipeable from 'react-native-swipeable';

class PendingTask extends Component {
    constructor(props) {
        super(props)
        this.currentlyOpenSwipeable = null
        this.state = {
            topListData: topList.data,
            bottomListData: bottomList.data,
            enableScroll: true
        }
    }

    renderTopRow = ({ index, item }) => {
        const { itemTopView, itemView, seprator, greenContainer, itemTitle, itemSubTitle, expText, grayContainer } = styles;
        return (
            <View style={itemView}>
                <View style={{ ...itemTopView, marginBottom: hp(0.8) }}>
                    <Image source={{ uri: item.user1.Image }} style={{ flex: 1 / 4.5, height: hp(9) }} resizeMode='contain'></Image>
                    <View style={{ flex: 1 / 2.5, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingHorizontal: wp(1) }}>
                        <View style={seprator}></View>
                        <View style={greenContainer}>
                            <Image source={{ uri: 'hand_white' }} style={{ height: hp(3), width: wp(6) }} resizeMode='contain'></Image>
                        </View>
                        <View style={seprator}></View>
                    </View>
                    <Image source={{ uri: item.user2.Image }} style={{ flex: 1 / 4.5, height: hp(9) }} resizeMode='contain'></Image>
                </View>
                <View style={itemTopView}>
                    <View style={{ flex: 1 / 3, alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={itemTitle} numberOfLines={1}>{item.user1.Name}</Text>
                        <Text style={itemSubTitle} numberOfLines={1}>{item.user1.Position}</Text>
                        <Text style={expText} numberOfLines={1}>{item.user1.Experiance}</Text>
                    </View>
                    <View style={{ flex: 1 / 3.5, flexDirection: 'row', alignItems: 'flex-start', justifyContent: 'center' }}>
                        <View style={grayContainer}>
                            <Image source={{ uri: 'chat' }} style={{ height: hp(2), width: wp(4) }} resizeMode='contain'></Image>
                        </View>
                        <View style={grayContainer}>
                            <Image source={{ uri: 'close_red' }} style={{ height: hp(1.5), width: wp(3) }} resizeMode='contain'></Image>
                        </View>
                    </View>
                    <View style={{ flex: 1 / 3, alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={itemTitle} numberOfLines={1}>{item.user2.Name}</Text>
                        <Text style={itemSubTitle} numberOfLines={1}>{item.user2.Position}</Text>
                        <Text style={expText} numberOfLines={1}>{item.user2.Experiance}</Text>
                    </View>
                </View>
                {/*  */}
            </View>
        )
    };

    renderBottomRow = ({ item, index }) => {
        const { itemTopView, itemBottomView, itemTitle, itemSubTitle, expText, seprator, itemBlurView } = styles;
        const itemProps = {
            onOpen: (event, gestureState, swipeable) => {
                if (this.currentlyOpenSwipeable && this.currentlyOpenSwipeable !== swipeable) {
                    this.currentlyOpenSwipeable.recenter();
                }
                this.currentlyOpenSwipeable = swipeable;
            },
            onClose: () => { this.currentlyOpenSwipeable = null }
        };
        const rightButtons = [
            <TouchableOpacity style={{ backgroundColor: Constant.color.green, flex: 1, justifyContent: 'center', padding: wp(7) }}>
                <Image source={{ uri: 'pin' }} style={{ height: hp(3), width: wp(6) }} resizeMode='contain'></Image>
            </TouchableOpacity>,
            <TouchableOpacity style={{ backgroundColor: Constant.color.red, flex: 1, justifyContent: 'center', padding: wp(8) }}>
                <Image source={{ uri: 'close_white' }} style={{ height: hp(2), width: wp(4) }} resizeMode='contain'></Image>
            </TouchableOpacity>];
        return (
            <Swipeable
            onSwipeStart={() => this.setState({enableScroll: false})}
                onSwipeRelease={() => this.setState({enableScroll: true})}
                rightButtons={rightButtons}
                onRightButtonsOpenRelease={itemProps.onOpen}
                onRightButtonsCloseRelease={itemProps.onClose}
                rightButtonWidth={wp(20)}
                key={index} >
                <View style={itemBottomView} >
                    <View style={itemTopView}>
                        <View style={{ flex: 1 / 3, justifyContent: 'center' }}>
                            <Text style={{ ...itemTitle, textAlign: 'right' }} numberOfLines={1}>{item.user1.Name}</Text>
                            <Text style={{ ...itemSubTitle, textAlign: 'right', width: wp(30) }} numberOfLines={1}>{item.user1.Position}</Text>
                            <Text style={{ ...expText, textAlign: 'right' }} numberOfLines={1}>{item.user1.Experiance}</Text>
                        </View>
                        <View style={{ flex: 1 / 3, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>

                            <Image source={{ uri: item.user1.Image }} style={{ height: wp(9), width: wp(9), borderRadius: wp(4.5) }} resizeMode='contain'></Image>
                            <View style={seprator}></View>
                            <Image source={{ uri: item.user2.Image }} style={{ height: wp(9), width: wp(9), borderRadius: wp(4.5) }} resizeMode='contain'></Image>

                        </View>
                        <View style={{ flex: 1 / 3, justifyContent: 'center' }}>
                            <Text style={{ ...itemTitle, textAlign: 'left' }} numberOfLines={1}>{item.user2.Name}</Text>
                            <Text style={{ ...itemSubTitle, textAlign: 'left', width: wp(30) }} numberOfLines={1}>{item.user2.Position}</Text>
                            <Text style={{ ...expText, textAlign: 'left' }} numberOfLines={1}>{item.user2.Experiance}</Text>
                        </View>
                    </View>
                </View>
                {!item.isAvailable &&
                    <View style={itemBlurView}>
                        <Image source={{ uri: 'close_red' }} style={{ height: hp(1.5), width: wp(3) }} resizeMode='contain'></Image>
                    </View>
                }

            </Swipeable>
        )
    }

    render() {
        const { headerView, warningText } = styles;
        const { topListData, bottomListData, enableScroll } = this.state;
        return (
            <ScrollView scrollEnabled={enableScroll} showsVerticalScrollIndicator={false} bounces={false}>
                <View style={headerView}>
                    <Image source={{ uri: 'warning' }} style={{ height: hp(5), width: wp(10), marginBottom: hp(1) }} resizeMode='contain'></Image>
                    <Text style={warningText}>{`You have already 3 tasks to complete.`}</Text>
                    <Text style={warningText}>{`Hurry and get on them before it's too late`}</Text>
                    <Text style={warningText}>{`to reverse your score.`}</Text>
                </View>
                <FlatList
                    showsVerticalScrollIndicator={false}
                    data={topListData}
                    renderItem={this.renderTopRow}
                    keyExtractor={(item, index) => {
                        return index + "";
                    }}
                    ItemSeparatorComponent={() => (
                        <View style={{ height: 1.5, backgroundColor: Constant.color.sepratorColor }} />
                    )}
                />
                <View style={{ height: 1.5, backgroundColor: Constant.color.sepratorColor }} />
                <FlatList
                    ref={ref => this._list = ref}
                    scrollEnabled={false}
                    showsVerticalScrollIndicator={false}
                    data={bottomListData}
                    renderItem={this.renderBottomRow}
                    keyExtractor={(item, index) => {
                        return index + "";
                    }}
                    ItemSeparatorComponent={() => (
                        <View style={{ height: 1.5, backgroundColor: Constant.color.sepratorColor }} />
                    )}
                />
                <View style={{ height: 1.5, backgroundColor: Constant.color.sepratorColor }} />
            </ScrollView>
        )
    }
}
const styles = StyleSheet.create({
    headerView: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Constant.color.grayBackground,
        paddingVertical: hp(2)
    },
    warningText: {
        marginTop: hp(0.5),
        textAlign: 'center',
        fontSize: Constant.fontSize.xsmall,
        color: Constant.color.warningFont,
        fontFamily: Constant.font.SFPro_Regular
    },
    itemView: {
        padding: wp(4),
        paddingHorizontal: wp(2)
    },
    itemTopView: {
        flexDirection: 'row',
        justifyContent: 'center'
    },
    seprator: {
        flex: 1 / 2.5,
        height: 2,
        backgroundColor: Constant.color.gray
    },
    greenContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        width: wp(14),
        height: wp(14),
        borderRadius: wp(7),
        backgroundColor: Constant.color.green
    },
    itemTitle: {
        textAlign: 'center',
        fontSize: Constant.fontSize.xsmall,
        color: Constant.color.black,
        fontWeight: '500',
        fontFamily: Constant.font.SFPro_Regular,
        width: wp(30)
    },
    itemSubTitle: {
        textAlign: 'center',
        fontSize: Constant.fontSize.xxsmall,
        color: Constant.color.warningFont,
        fontFamily: Constant.font.SFPro_Regular,
        width: wp(32)
    },
    expText: {
        textAlign: 'center',
        fontSize: Constant.fontSize.xmini,
        color: Constant.color.lightGrayText,
        fontFamily: Constant.font.SFPro_Regular,
        width: wp(30)
    },
    grayContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        width: wp(9),
        height: wp(9),
        borderRadius: wp(4.5),
        backgroundColor: '#E2E2E2',
        marginHorizontal: wp(1)
    },
    itemBottomView: {
        padding: wp(4),
        backgroundColor: Constant.color.grayBackground
    },
    itemBlurView: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        right: 0,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fffffdd0',
        height: hp(10),
        width: '100%'
    }
});

export default PendingTask;
