import React, { Component } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    Image,
    TouchableOpacity
} from 'react-native';
import { wp, hp } from '../helper/responsiveScreen';
import Constant from '../helper/themeHelper';
import PendingTask from './PendingTask'

class Task extends Component {
    constructor(props) {
        super(props)
        this.state = {
            activeTab: 'PendingTask'
        }
    }

    render() {
        const { tabBarRow, tab, tabTitle, activeDot } = styles;
        const { activeTab } = this.state;
        return (
            <View style={{flex: 1}}>
                <View style={tabBarRow}>
                    <TouchableOpacity style={{ ...tab, borderBottomColor: activeTab == 'PendingTask' ? Constant.color.yellow : Constant.color.white }} onPress={() => this.setState({ activeTab: 'PendingTask' })} >
                        <Text style={{ ...tabTitle, color: activeTab == 'PendingTask' ? Constant.color.grayFont : Constant.color.lightGray }}>PENDING TASKS</Text>
                        {activeTab == 'PendingTask' &&
                            <View style={activeDot}></View>
                        }
                    </TouchableOpacity>
                    <TouchableOpacity style={{ ...tab, borderBottomColor: activeTab == 'CompletedTask' ? Constant.color.yellow : Constant.color.white }} onPress={() => this.setState({ activeTab: 'CompletedTask' })} >
                        <Text style={{ ...tabTitle, color: activeTab == 'CompletedTask' ? Constant.color.grayFont : Constant.color.lightGray }}>COMPLETED TASKS</Text>
                        {activeTab == 'CompletedTask' &&
                            <View style={activeDot}></View>
                        }
                    </TouchableOpacity>
                </View>
                {activeTab == 'PendingTask' && <PendingTask/>}
                {activeTab == 'CompletedTask' && <View/>}
            </View>
        )
    }
}
const styles = StyleSheet.create({
    tabTitle: {
        fontSize: Constant.fontSize.mini,
        color: Constant.color.grayFont,
        fontFamily: Constant.font.SFPro_Bold,
        fontWeight: 'bold'
    },
    tabBarRow: {
        flexDirection: 'row',
        backgroundColor: Constant.color.white,
        alignItems: 'center'
    },
    tab: {
        flex: 1 / 2,
        flexDirection:'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: hp(1.5),
        borderBottomWidth: 2,
        marginHorizontal: wp(5)
    },
    activeDot: {
        height:hp(1),
        width: hp(1),
        borderRadius: hp(0.5),
        backgroundColor: Constant.color.red,
        marginLeft: wp(2)
    }
});

export default Task;
