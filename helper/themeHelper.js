import { Platform, Dimensions, PixelRatio } from 'react-native';

const {
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
} = Dimensions.get('window');

const isIOS = (Platform.OS === 'ios');
const d = Dimensions.get("window")
const isiPAD = ((SCREEN_HEIGHT/SCREEN_WIDTH) < 1.6)

// based on iphone 5s's scale
const scale = SCREEN_WIDTH / 375;

export function normalize(size) {
    const newSize = size * scale;
    if (Platform.OS === 'ios') {
        return Math.round(PixelRatio.roundToNearestPixel(newSize))
    } else {
        return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2
    }
}
module.exports = {
    //API Constant
    color:{
        yellow: '#F2994A',
        lightGray: '#E8E8E8',
        white: '#ffffff',
        black: '#000000',
        red: '#D0021B',
        gray: '#D9D9D9',
        green: '#43B681',
        grayBackground: '#F8F8F8',
        
        sepratorColor: '#E8E8E8',

        blackFont: '#080808',
        grayFont: '#727270',
        warningFont: '#323E46',
        lightGrayText: '#C3C3C3'

    },
    style:{
        container:{width:SCREEN_WIDTH*0.85,alignSelf:'center'}
    },
    screen: Dimensions.get('window'),
    isIOS: isIOS,
    isANDROID: Platform.OS === 'android',
    isiPAD: isiPAD,
    isX : Platform.OS === "ios" && (d.height > 800 || d.width > 800) ? true : false,

    screenHeight:  isIOS && SCREEN_HEIGHT || SCREEN_HEIGHT - 24,
    screenWidth:  SCREEN_WIDTH,
    fullScreenHeight:  SCREEN_HEIGHT,
    font:{
        SFPro_Regular: isIOS && 'SFProText-Regular' || 'SFProText_Regular',
        SFPro_Medium: isIOS && 'SFProText-Medium' || 'SFProText_Medium',
        SFPro_Bold: isIOS && 'SFProText-Bold' || 'SFProText_Bold',
    },
    fontSize:{
        xmini: normalize(10),
        mini: normalize(12),
        xxsmall: normalize(14),
        xsmall: normalize(15),
        small: normalize(16),
        xmedium: normalize(18),
        medium: normalize(20),
        large: normalize(21),
        xlarge: normalize(24),
    },


    shadowStyle:{
        shadowColor: '#c1c1c1',
        shadowOffset: {width: 1, height: 2},
        shadowOpacity: 1,
        shadowRadius: 5,
        elevation: 2,
        zIndex: 10000000
    },

    gradientColours: {
        green: ['transparent', '#245250'],
        darkGreen: ['transparent', '#0D4354'],
        blue: ['transparent', '#305489'],
        lightBlue: ['transparent', '#2D80D7'],
        darkBlue: ['transparent', '#093C72'],
    },

    loaderBackground: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'space-around',
        backgroundColor: 'rgba(52, 52, 52, 0.5)'
    }
};
