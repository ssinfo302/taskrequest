import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Image,
  TouchableOpacity
} from 'react-native';
import { wp, hp } from './helper/responsiveScreen';
import Constant from './helper/themeHelper';
import Task from './Screens/Task';

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      activeTab: 'task'
    }
  }

  render() {
    const { header, title, topImageRow, topImageView, container } = styles;
    const { activeTab } = this.state;
    return (
      <SafeAreaView style={container}>
        <View style={header}>
          <Image source={{ uri: 'bell' }} style={{ height: hp(3.5), width: wp(8) }} resizeMode='contain'></Image>
          <Text style={title}>Task Request</Text>
          <Image source={require('./assets/images/User.png')} style={{ height: hp(3), width: wp(7) }} resizeMode='contain'></Image>
        </View>
        <View style={topImageRow}>
          <TouchableOpacity onPress={() => this.setState({ activeTab: 'path' })} style={{ ...topImageView, borderBottomColor: activeTab == 'path' ? 'white' : Constant.color.lightGray }}>
            <Image source={{ uri: 'path' }} style={{ height: hp(3.5), width: wp(8) }} resizeMode='contain'></Image>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.setState({ activeTab: 'tie' })} style={{ ...topImageView, borderBottomColor: activeTab == 'tie' ? 'white' : Constant.color.lightGray }}>
            <Image source={{ uri: 'tie' }} style={{ height: hp(3.5), width: wp(8) }} resizeMode='contain'></Image>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.setState({ activeTab: 'task' })} style={{ ...topImageView, borderBottomColor: activeTab == 'task' ? 'white' : Constant.color.lightGray }}>
            <Image source={{ uri: 'hand' }} style={{ height: hp(3.5), width: wp(8) }} resizeMode='contain'></Image>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.setState({ activeTab: 'userSearch' })} style={{ ...topImageView, borderBottomColor: activeTab == 'userSearch' ? 'white' : Constant.color.lightGray }}>
            <Image source={{ uri: 'user_search' }} style={{ height: hp(3.5), width: wp(8) }} resizeMode='contain'></Image>
          </TouchableOpacity>
        </View>
        {activeTab == 'path' && <View></View>}
        {activeTab == 'tie' && <View></View>}
        {activeTab == 'task' &&
          <Task />
        }
        {activeTab == 'userSearch' && <View></View>}
      </SafeAreaView>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Constant.color.white
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: Constant.color.white,
    alignItems: 'center',
    paddingHorizontal: wp(5),
    paddingVertical: hp(1)
  },
  title: {
    fontSize: Constant.fontSize.large,
    color: Constant.color.blackFont,
    fontFamily: Constant.font.SFPro_Medium
  },
  topImageRow: {
    flexDirection: 'row',
    backgroundColor: Constant.color.white,
    alignItems: 'center'
  },
  topImageView: {
    flex: 1 / 4,
    paddingVertical: hp(1),
    borderBottomWidth: 0.5,
    alignItems: 'center',
    justifyContent: 'center'
  }
});

console.disableYellowBox = true;

export default App;
